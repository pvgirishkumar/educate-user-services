package com.eductate.user.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.eductate.user.enums.AuthProvider;

@Entity
@Table(name = "user_auth")
public class UserAuth extends BaseObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = APP_SERIAL_ID;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="user_id")
	private User user;
	
	private String password;
	
	private boolean active = true;
	
	private boolean resetRequested = false;
	
	private String tempPassword;
	
	private AuthProvider authProvider;
	
	private String authProviderId;
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isResetRequested() {
		return resetRequested;
	}

	public void setResetRequested(boolean resetRequested) {
		this.resetRequested = resetRequested;
	}

	public String getTempPassword() {
		return tempPassword;
	}

	public void setTempPassword(String tempPassword) {
		this.tempPassword = tempPassword;
	}

	public AuthProvider getAuthProvider() {
		return authProvider;
	}

	public void setAuthProvider(AuthProvider authProvider) {
		this.authProvider = authProvider;
	}

	public String getAuthProviderId() {
		return authProviderId;
	}

	public void setAuthProviderId(String authProviderId) {
		this.authProviderId = authProviderId;
	}


}
