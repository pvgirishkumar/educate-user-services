package com.eductate.user.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "user_address")
public class UserAddress extends BaseObject {
	
	private static final long serialVersionUID = APP_SERIAL_ID;
	
	@Column
	private String userKey;
	
	@Column
	private String purpose;

	@Column
	private String carrierBranchId;

	@Column
	private String carrierCustomerId;

	@Column(name="customer_name")
	private String name;

	@Column
	private String firstName;

	@Column
	private String lastName;

	@Column
	private String city;

	@Column
	private String addressAddition;

	@Column
	private String postalCode;

	@Column
	private String postBox;

	@Column
	private String state;

	@Column
	private String country;

	@Column
	private String street;

	@Column
	private String houseNumber;

	@Column(name="is_primary")
	private boolean primary = false;

	@Column
	private String email;

	@Column
	private String phone;


}
