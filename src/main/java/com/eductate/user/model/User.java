package com.eductate.user.model;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.eductate.user.enums.UserType;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "user")
public class User extends BaseObject {

	private static final long serialVersionUID = APP_SERIAL_ID;

	@Column(name = "user_key", nullable = true, unique = true)
	private String userKey;

	private String firstName;

	private String lastName;

	@Column(name = "email", unique = true)
	private String email;

	@Column(name = "phone_number")
	private String phoneNumber;

	@Enumerated(EnumType.STRING)
	private UserType userType;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "user", fetch = FetchType.EAGER)
	private Set<UserRole> roles;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "user", fetch = FetchType.EAGER)
	private List<UserAuth> userAuths;

	private Boolean active = false;

	private Long registrationStep = 0L;

	@OneToOne(mappedBy = "user", fetch = FetchType.LAZY)
	private UserProfile profile;

	private Boolean deleted = false;

	private Date deletedOn;
	
	@Column(name = "gender")
	private String gender;
	
	@Column(name = "dob")
	private Date dateOfBirth;
	
	@Column(name = "phone_number2")
	private String phoneNumber2;
	
	@Column(name = "customer_id")
	private String customerId;
	
	@Column(name = "activation_token")
	private String activationToken;
	
	@Column(name = "activatio_link_sent_on")
	private Date activatioLinkSentOn;
	
	@Column(name = "source")
	private String source;
	
	@Column(name = "title")
	private String title;
}
