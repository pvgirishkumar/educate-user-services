package com.eductate.user.dto;

import java.io.Serializable;
import java.util.Date;

import com.eductate.user.utils.EduConstants;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class BaseDto implements Serializable, EduConstants {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = APP_SERIAL_ID;
	
	private Long id;
	
	private Date lastUpdatedOn;
	
	private Date createdOn;
	
	private String message = SUCCESS;
	
	private Integer responseCode = SUCCESS_CODE;

}
