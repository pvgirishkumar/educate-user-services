package com.eductate.user.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UserProfileDto extends BaseDto {
	
	private static final long serialVersionUID = APP_SERIAL_ID;
	
	private String userKey;
	
	private String user;
	
	private String companyName;
	
	private String jobTitle;
	
	private String profilePicture;
	
}
