package com.eductate.user.dto;


import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;

import com.eductate.user.enums.Role;
import com.eductate.user.enums.UserType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;


@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class UserDto extends BaseDto implements UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = APP_SERIAL_ID;
	
	private String userKey;

	private String firstName;
	
	private String lastName;
	
	private String email;
	
	private UserType userType;
		
	private Set<Role> roles;
	
	private String token;
	
	private String tempToken;
	
	private String phoneNumber;
	
	private String phoneNumber2;
	
	private Date dateofBirth;
	
	private String gender;
	
	private String customerId;
	
	private String activationToken;
	
	private Boolean active;
	
	private Boolean userExist;
	
	private String baseUrl;
	
	private String source;
	
	private String title;
	
	@JsonIgnore
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return AuthorityUtils.commaSeparatedStringToAuthorityList(StringUtils.collectionToCommaDelimitedString(roles));
	}
	
	public void addRole(Role role) {
		if (this.roles == null) {
			this.roles = new HashSet<Role>();
		}
		this.roles.add(role);
	}
	
	@JsonIgnore
	@Override
	public String getPassword() {
		return token;
	}

	@JsonIgnore
	@Override
	public String getUsername() {
		return email;
	}

	@JsonIgnore
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@JsonIgnore
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@JsonIgnore
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@JsonIgnore
	@Override
	public boolean isEnabled() {
		return true;
	}

}
