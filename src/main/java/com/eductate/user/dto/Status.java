package com.eductate.user.dto;

public class Status {

	private Integer code = BaseDto.SUCCESS_CODE;
	private String message = "";
	private Boolean shouldDisplay;

	public Status() {
	}

	public Status(Integer code, String message, Boolean shouldDisplay) {
		this.code = code;
		this.message = message;
		this.shouldDisplay = shouldDisplay;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Boolean isShouldDisplay() {
		return shouldDisplay;
	}

	public void setShouldDisplay(Boolean shouldDisplay) {
		this.shouldDisplay = shouldDisplay;
	}
}
