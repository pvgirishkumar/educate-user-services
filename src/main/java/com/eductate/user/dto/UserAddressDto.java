package com.eductate.user.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Setter
@Getter
@ToString
public class UserAddressDto extends BaseDto {
	
	private static final long serialVersionUID = APP_SERIAL_ID;
	
	private String userKey;
	
	private String purpose;

	private String carrierBranchId;

	private String carrierCustomerId;

	private String name;

	private String firstName;

	private String lastName;

	private String city;

	private String addressAddition;

	private String postalCode;

	private String postBox;

	private String state;

	private String country;

	private String street;

	private String houseNumber;

	private boolean primary;

	private String email;

	private String phone;
	
}
