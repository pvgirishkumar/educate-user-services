package com.eductate.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eductate.user.dto.BaseDto;
import com.eductate.user.dto.ChangePasswordDto;
import com.eductate.user.dto.UserDto;
import com.eductate.user.service.IUserService;


@RestController
@RequestMapping("/user")
public class UserController  {
	
	@Autowired
	private IUserService userService;
	
	
	@PostMapping("register")
	public Object registerUser(@RequestHeader(name = "country", required = false, defaultValue = "IND") String country,
			@RequestHeader(name = "language", required = false, defaultValue = "en") String language,
			@RequestBody UserDto source) {
		return userService.registerUser(country, language, source);
	}
	
	@PostMapping("forgot-password")
	public UserDto forgotPassword (@RequestHeader(name = "country", required = false, defaultValue = "IND") String country,
			@RequestHeader(name = "language", required = false, defaultValue = "en") String language,
			@RequestBody UserDto source) {
		return userService.forgotPassword(country, language, source);
	}
	
	@GetMapping("{userKey}")
	public UserDto getUser(
			@RequestHeader(name = "country", required = false, defaultValue = "IND") String country,
			@RequestHeader(name = "language", required = false, defaultValue = "en") String language,
			@PathVariable(name="userKey") String userKey) {
		return userService.getUserDetails(userKey);
	}
	
	@PutMapping("{userKey}/update-profile")
	public UserDto updateUser(
			@RequestHeader(name = "country", required = false, defaultValue = "IND") String country,
			@RequestHeader(name = "language", required = false, defaultValue = "en") String language,
			@PathVariable(name="userKey") String userKey, @RequestBody UserDto user) {
		user.setUserKey(userKey);
		return userService.updateUserProfile(user);
	}
	
	@PostMapping("{userKey}/change-password")
	public BaseDto changePassword(
			@RequestHeader(name = "country", required = false, defaultValue = "IND") String country,
			@RequestHeader(name = "language", required = false, defaultValue = "en") String language,
			@RequestBody ChangePasswordDto source, @PathVariable(name="userKey") String userKey) {
		return userService.changePassword(source, userKey);
	}
	
	@PostMapping("/reset-password")
	public BaseDto resetPassword(
			@RequestHeader(name = "country", required = false, defaultValue = "IND") String country,
			@RequestHeader(name = "language", required = false, defaultValue = "en") String language,
			@RequestBody ChangePasswordDto source) {
		return userService.resetPassword(source);
	}
	
	@PutMapping("update-customer-id")
	public BaseDto updateCustomerId(
			@RequestHeader(name = "country", required = false, defaultValue = "IND") String country,
			@RequestHeader(name = "language", required = false, defaultValue = "en") String language,
			@RequestBody UserDto source) {
		return userService.updateCustomerId(source.getEmail(), source.getCustomerId());
	}
	
	@GetMapping("find-by-email/{email}")
	public UserDto findByEmail(
			@RequestHeader(name = "country", required = false, defaultValue = "IND") String country,
			@RequestHeader(name = "language", required = false, defaultValue = "en") String language,
			@PathVariable(name="email") String email) {
		return userService.findByEmail(email);
	}
	
	@PostMapping("/user-exist")
	public UserDto checkUserExist(
			@RequestHeader(name = "country", required = false, defaultValue = "IND") String country,
			@RequestHeader(name = "language", required = false, defaultValue = "en") String language,
			@RequestBody UserDto source) {
		return userService.checkUserExist(source);
	}
	
	
}
