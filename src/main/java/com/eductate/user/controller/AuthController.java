package com.eductate.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.eductate.user.dto.BaseDto;
import com.eductate.user.dto.ChangePasswordDto;
import com.eductate.user.dto.UserDto;
import com.eductate.user.service.IUserService;

import lombok.extern.slf4j.Slf4j;


@RestController
@Slf4j
public class AuthController {
	
	@Autowired
	private IUserService userService;
	
	@PostMapping("forgot-password")
	public UserDto forgotPassword (@RequestHeader(name = "country", required = false, defaultValue = "IND") String country,
			@RequestHeader(name = "language", required = false, defaultValue = "en") String language,
			@RequestBody UserDto source) {
		return userService.forgotPassword(country, language, source);
	}
	
	@PostMapping("reset-password")
	public @ResponseBody BaseDto resetPassword(@RequestBody ChangePasswordDto source) {
		return userService.resetPassword(source);
	}
	
	@GetMapping("activate-user")
	public @ResponseBody Object activateUser(@RequestParam(name = "token", required = true) String token) {
		return userService.activateUser(token);
	}
	
	@GetMapping("send-activation-email")
	public @ResponseBody BaseDto sendActivationEmail(@RequestHeader(name = "country", required = false, defaultValue = "IND") String country,
			@RequestHeader(name = "language", required = false, defaultValue = "en") String language,
			@RequestParam(name = "email", required = false) String email, @RequestParam(name = "domain", required = false) String domain) {
		return userService.sendActivationEmail(country, language, email, domain);
	}
}
