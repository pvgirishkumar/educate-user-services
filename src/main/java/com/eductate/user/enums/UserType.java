package com.eductate.user.enums;

public enum UserType {
	
	USER, ADMIN, SOCIAL_USER, STAFF, MODERATOR, TEACHER, STUDENT, TUTOR;

}
