package com.eductate.user.enums;


public enum Role {
	USER, ADMIN, STUDENT, TEACHER;
	
	public static Role fromString (String value) {
		Role role = null;
		for ( Role r :Role.values()) {
			if ( r.name().equalsIgnoreCase(value)) {
				role = r;
				break;
			}
		}
		return role;
	}
}
