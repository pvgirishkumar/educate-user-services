package com.eductate.user.enums;

public enum AddressType {
	
	//SHIPPING, BILLING
	HOME, OFFICE, DELIVERY, PAYMENT, SHIPPING, BILLING
}
