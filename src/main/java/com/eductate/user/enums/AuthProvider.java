package com.eductate.user.enums;

public enum AuthProvider {
	
	local,
    facebook,
    google,
    linkedin

}
