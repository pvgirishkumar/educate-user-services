package com.eductate.user.service.impl;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerEndpointsConfiguration;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.eductate.user.dto.BaseDto;
import com.eductate.user.dto.ChangePasswordDto;
import com.eductate.user.dto.UserDto;
import com.eductate.user.enums.AuthProvider;
import com.eductate.user.enums.Role;
import com.eductate.user.exception.UserNotFoundException;
import com.eductate.user.exception.ValidationException;
import com.eductate.user.model.User;
import com.eductate.user.model.UserAuth;
import com.eductate.user.model.UserRole;
import com.eductate.user.repository.UserAuthRepository;
import com.eductate.user.repository.UserRepository;
import com.eductate.user.service.IUserService;
import com.eductate.user.utils.BeanConverter;
import com.eductate.user.utils.CommonUtilities;

import lombok.extern.slf4j.Slf4j;

@Service
@Transactional(readOnly = true)
@Slf4j
public class UserService implements IUserService {
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
    private AuthorizationServerEndpointsConfiguration configuration;
	
	@Autowired
	private UserAuthRepository userAuthRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UserNotFoundException {
		Optional<User> optUser = userRepository.findByUserName(username);
		UserDto userDto = null;
		if (optUser.isPresent()) {
			User user = optUser.get();
			if(user.getDeleted() != null && user.getDeleted()) {
				throw new UserNotFoundException("Username: " + username + " not found");
			}
			userDto = BeanConverter.toUserDto(user);
			UserAuth auth = null;
			for (UserAuth a : user.getUserAuths()) {
				if(a.isActive()) {
					auth = a;
					break;
				}
			}
			if (auth != null) {
				userDto.setToken(auth.getPassword());
			} else {
				userDto = null;
				
			}
		}
		if(userDto == null) {
			throw new UserNotFoundException("Username: " + username + " not found");
		}
		return userDto;
	}
	
	@Override
	public Map<String, Boolean> checkUserExist(String email, String mobileNumber) {
		Map<String, Boolean> result = new HashMap<>();
		Boolean emailExist = false;
		if (StringUtils.hasText(email)) {
			emailExist = userRepository.findByUserName(email).isPresent();
		}
		result.put(EMAIL, emailExist);
		return result;
	}

	@Transactional
	@Override
	public Object registerUser(String country, String language, UserDto userDto) {
		Map<String, Boolean> result = checkUserExist(userDto.getEmail(), null);
		if (result.get(EMAIL)) {
			throw new ValidationException("User already exist");
		}
		User user = BeanConverter.toUser(userDto, new User());
		List<UserAuth> authList = new ArrayList<>();
		if ( StringUtils.hasText(userDto.getEmail())) {
			authList.add(prepareUserAuth(user, AuthProvider.local, userDto.getEmail(), userDto.getToken()));
		}
		user.setUserAuths(authList);
		Set<UserRole> userRoles = new HashSet<>();
		userRoles.add(prepareUserRole(user, Role.USER));
		user.setRoles(userRoles);
		user.setActive(true);
		user.setRegistrationStep(0L);
		user.setSource(userDto.getSource());
		user.setTitle(userDto.getTitle());
		user.setUserType(userDto.getUserType());

		userRepository.save(user);
		user.setUserKey(CommonUtilities.getUniqueId(16, user.getId()));
		user.setActivationToken(CommonUtilities.getUniqueId(10, user.getId()));
		userDto.setActivationToken(user.getActivationToken());
		userDto.setActive(false);
		
        return getOauthToken(BeanConverter.toUserDto(user));
	}

	private OAuth2AccessToken getOauthToken(UserDto source) {
        OAuth2AccessToken token = null;
        ClientDetails clientDetails = configuration.getEndpointsConfigurer().getClientDetailsService()
                .loadClientByClientId("api");
        AuthorizationServerTokenServices tokenService = configuration.getEndpointsConfigurer().getTokenServices();
        Map<String, String> requestParameters = new HashMap<>();
        Map<String, Serializable> extensionProperties = new HashMap<>();

        boolean approved = true;
        Set<String> responseTypes = new HashSet<>(Arrays.asList("code"));

        OAuth2Request oauth2Request = new OAuth2Request(requestParameters, clientDetails.getClientId(),
                clientDetails.getAuthorities(), approved, clientDetails.getScope(), clientDetails.getResourceIds(),
                null, responseTypes, extensionProperties);
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(source,
                source.getToken(), clientDetails.getAuthorities());

        OAuth2Authentication oAuth2Authentication = new OAuth2Authentication(oauth2Request, authenticationToken);
        token = tokenService.createAccessToken(oAuth2Authentication);

        return token;
    }

	private UserAuth prepareUserAuth(User user, AuthProvider authProvider, String username, String token) {
		UserAuth auth = new UserAuth();
		auth.setActive(true);
		auth.setAuthProvider(authProvider);
		auth.setAuthProviderId(username);
		if ( StringUtils.hasText(token)) {
			auth.setPassword(passwordEncoder.encode(token));
		}
		auth.setUser(user);
		return auth;
	}
	
	private UserRole prepareUserRole(User user, Role role) {
		UserRole userRole = new UserRole();
		userRole.setRole(role);
		userRole.setUser(user);
		return userRole;
	}

	@Override
	public UserDto getUserDetails(String userKey) {
		if(userKey !=  null) {
			Optional<User> wrapper = userRepository.findByUserKey(userKey);
			if(wrapper.isPresent()) {
				UserDto target = BeanConverter.toBaseUserDto(wrapper.get());				
				return target;
			}
		}
		return null;
	}
	
	@Transactional
	@Override
	public UserDto updateUserProfile(UserDto source) {
		if(source.getUserKey() != null) {
			Optional<User> wrapper = userRepository.findByUserKey(source.getUserKey());
			if(wrapper.isPresent()) {
				User target = wrapper.get();
				if(source.getFirstName() != null) {
					target.setFirstName(source.getFirstName());
				}
				if(source.getLastName() != null) {
					target.setLastName(source.getLastName());
				}
				if(source.getPhoneNumber() != null) {
					target.setPhoneNumber(source.getPhoneNumber());
				}
				
				if(source.getTitle() != null) {
					target.setTitle(source.getTitle());
				}

				userRepository.save(target);
			}
		}
		return source;
    }

	@Transactional
	@Override
	public BaseDto resetPassword(ChangePasswordDto source) {
		// TODO Auto-generated method stub
		BaseDto result = new BaseDto();
		
		Optional<UserAuth> authWrapper = userAuthRepository.findByTempPassword(source.getToken());
		if (authWrapper.isPresent()) {
			UserAuth userAuth = authWrapper.get();
			if (!userAuth.isResetRequested()) {
				result.setMessage("Invalid token");
				return result;
			}
			UserAuth auth = new UserAuth();
			auth.setUser(userAuth.getUser());
			auth.setCreatedOn(new Date());
			auth.setLastUpdatedOn(new Date());
			auth.setActive(true);
			auth.setResetRequested(false);
			auth.setPassword(passwordEncoder.encode(source.getPassword()));
			auth.setAuthProvider(userAuth.getAuthProvider());
			auth.setAuthProviderId(userAuth.getAuthProviderId());
			userAuthRepository.save(auth);
			
			userAuth.setActive(false);
			userAuth.setResetRequested(false);
			userAuth.setLastUpdatedOn(new Date());
			userAuthRepository.save(userAuth);
			result.setMessage("Your password has been successfully reset.");
			result.setResponseCode(BaseDto.SUCCESS_CODE);
		} else {
			result.setMessage("Invalid token");
		}
		return result;
	}

	@Transactional
	@Override
	public BaseDto changePassword(ChangePasswordDto source, String userKey) {
		BaseDto result = new BaseDto();

		Optional<User> wrapper = userRepository.findByUserKey(userKey);
		result.setResponseCode(BaseDto.ERROR_CODE);
		if (wrapper.isPresent()) {
			User user = wrapper.get();
			Optional<UserAuth> authWrapper = userAuthRepository.findActiveAuth(user.getId());
			if (authWrapper.isPresent()) {
				UserAuth userAuth = authWrapper.get();
				String pass = userAuth.getPassword();
				
				if (passwordEncoder.matches(source.getOldPassword(), pass)) {
					userAuthRepository.deActivateAuth(user.getId(), new Date());
					UserAuth auth = new UserAuth();
					auth.setUser(user);
					auth.setCreatedOn(new Date());
					auth.setLastUpdatedOn(new Date());
					auth.setActive(true);
					auth.setAuthProvider(userAuth.getAuthProvider());
					auth.setAuthProviderId(userAuth.getAuthProviderId());
					auth.setResetRequested(false);
					auth.setPassword(passwordEncoder.encode(source.getNewPassword()));
					userAuthRepository.save(auth);
					result.setMessage("Your password has been successfully changed.");
					result.setResponseCode(BaseDto.SUCCESS_CODE);
				} else {
					result.setMessage("Enter your old password.");
				}
			} else {
				result.setMessage("Error in changing password");
			}
		} else {
			result.setMessage("Error in changing password");
		}
		return result;
	}
	
	@Override
	@Transactional
	public UserDto forgotPassword(String country, String language, UserDto source) {
		Optional<User> wrapper = userRepository.findByUserName(source.getEmail());
		source.setResponseCode(BaseDto.ERROR_CODE);
		if (wrapper.isPresent()) {
			User user = wrapper.get();
			UserAuth userAuth = null;
			for (UserAuth auth : user.getUserAuths()) {
				if (auth.isActive()) {
					userAuth = auth;
				}
			}
			if (userAuth != null) {
				String tempPassword = CommonUtilities.getUniqueId(18, user.getId());
				userAuth.setTempPassword(tempPassword);
				userAuth.setLastUpdatedOn(new Date());
				userAuth.setResetRequested(true);
				userAuthRepository.saveAndFlush(userAuth);
				source.setTempToken(tempPassword);
				source.setFirstName(user.getFirstName());
				source.setLastName(user.getLastName());
				source.setEmail(user.getEmail());
				//emailService.sendForgotPasswordEmail(source, country, language, source.getBaseUrl());
				// sendResetLink(source);
				source.setResponseCode(BaseDto.SUCCESS_CODE);
				source.setMessage("We have sent a link to your email to reset your password.");
			}

		} else {
			source.setMessage("Sorry, we are not able to find your account.");
			//source.setShouldDisplay(true);
		}
		return source;
	}
	
	@Override
	@Transactional
	public Object activateUser(String activationToken) {
		BaseDto target = new BaseDto();
		Optional<User> userWrapper = userRepository.findByActivationToken(activationToken);
		if(userWrapper.isPresent()) {
			User user = userWrapper.get();			
			user.setActive(true);
			user.setLastUpdatedOn(new Date());
			user.setRegistrationStep(2L);
			userRepository.save(user);
			target.setMessage("User has been activated successfully.");
			UserDto userDto = BeanConverter.toUserDto(user);
			return getOauthToken(userDto);
			
		}else {
			target.setMessage("Invalid or expired token.");
			target.setResponseCode(BaseDto.ERROR_CODE);
		}
		return target;
	}

	@Override
	@Transactional
	public BaseDto updateCustomerId(String email, String customerId) {
		Optional<User> wrapper = userRepository.findByEmail(email);
		if(wrapper.isPresent()) {
			User user = wrapper.get();
			user.setCustomerId(customerId);
			user.setLastUpdatedOn(new Date());
			userRepository.save(user);
		}
		return new BaseDto();
	}

	@Override
	public UserDto findByEmail(String email) {
		if(email !=  null) {
			Optional<User> wrapper = userRepository.findByEmail(email);
			if(wrapper.isPresent()) {
				UserDto target = BeanConverter.toBaseUserDto(wrapper.get());				
				return target;
			}
		}
		return null;
	}
	
	@Override
	@Transactional
	public BaseDto sendActivationEmail(String country, String language, String email, String baseUrl) {
		email = email.replaceAll(" ", "+");
		log.info("SendActivationEmail email is: {}", email);
		BaseDto target = new BaseDto();
		Optional<User> userWrapper = userRepository.findByEmail(email);
		if(userWrapper.isPresent()) {
			User user = userWrapper.get();
			String token = CommonUtilities.getUniqueId(16, user.getId());
			UserDto userDto = BeanConverter.toUserDto(user);
			userDto.setToken(token);
			userDto.setActivationToken(token);
			//emailService.sendUserSignupEmail(userDto, country, language, baseUrl);
			user.setActivationToken(token);
			user.setActivatioLinkSentOn(new Date());
			user.setRegistrationStep(1L);
			userRepository.save(user);
			target.setMessage("Activation email has been sent successfully.");
			
		}else {
			target.setMessage("User not found.");
			target.setResponseCode(BaseDto.ERROR_CODE);
		}
		return target;
	}

	@Override
	public UserDto checkUserExist(UserDto userDto) {
		Optional<User> wrapper = userRepository.findByEmail(userDto.getEmail());
		if(wrapper.isPresent()) {
			userDto.setUserExist(true);
			userDto.setActive(wrapper.get().getActive());
		}else {
			userDto.setUserExist(false);
		}
		return userDto;
	}
}
