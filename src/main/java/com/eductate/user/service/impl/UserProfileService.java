package com.eductate.user.service.impl;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eductate.user.dto.UserProfileDto;
import com.eductate.user.model.UserProfile;
import com.eductate.user.repository.UserProfileRepository;
import com.eductate.user.service.IUserProfileService;
import com.eductate.user.utils.BeanConverter;
import com.eductate.user.utils.CommonUtilities;

@Service
@Transactional(readOnly = true)
public class UserProfileService implements IUserProfileService {
	
	@Autowired
	private UserProfileRepository userProfileRepo;
	
	@Override
	@Transactional
	public UserProfileDto createUserProfile(UserProfileDto source) {
		UserProfile entity = new UserProfile();
		BeanUtils.copyProperties(source, entity);
		userProfileRepo.save(entity);
		entity.setUserKey(CommonUtilities.getUniqueId(16, entity.getId()));
		source.setUserKey(entity.getUserKey());
		return source;
	}

	@Override
	@Transactional
	public UserProfileDto updateUserProfile(UserProfileDto source) {
		Optional<UserProfile> entity = userProfileRepo.findByUserProfile(source.getUserKey());
		if(entity.isPresent()) {
			UserProfile target = entity.get();
			BeanUtils.copyProperties(source, target, "id");
			userProfileRepo.save(target);
		}
		return source;
	}

	@Override
	public UserProfileDto getUserProfile(String userKey) {
		Optional<UserProfile> entity = userProfileRepo.findByUserProfile(userKey);
		UserProfile target = null;
		if(entity.isPresent()) {
			target = entity.get();
		}
		return BeanConverter.toUserProfileDto(target);
	}


}
