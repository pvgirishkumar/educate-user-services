package com.eductate.user.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eductate.user.dto.BaseDto;
import com.eductate.user.dto.UserAddressDto;
import com.eductate.user.model.UserAddress;
import com.eductate.user.repository.UserAddressRepository;
import com.eductate.user.service.IUserAddressService;
import com.eductate.user.utils.BeanConverter;


@Service
@Transactional(readOnly = true)
public class UserAddressService implements IUserAddressService  {
	
	private static Logger LOGGER = LoggerFactory.getLogger(UserAddressService.class);
	
	@Autowired
	private UserAddressRepository userAddressRepository;
	
	@Transactional
	@Override
	public UserAddressDto saveAddress(UserAddressDto source) {
		UserAddress target = null;
		if(source.getId() != null) {
			Optional<UserAddress> wrapper = userAddressRepository.findById(source.getId());
			if(wrapper.isPresent()) {
				target = wrapper.get();
			}
		}
		if(target == null) {
			target = new UserAddress();
			target.setCreatedOn(new Date());
		}
		target.setLastUpdatedOn(new Date());
		BeanConverter.copyUserAddressDto(source, target);
		userAddressRepository.save(target);
		source.setId(target.getId());
		return source;
	}
	
	@Override
	public List<UserAddressDto> getUserAddresses(String userId) {
		if(userId != null) {
			List <UserAddress> list = userAddressRepository.findByUserKey(userId);
			if(list != null && !list.isEmpty()) {
				return list.stream().map(item -> BeanConverter.toUserAddressDto(item)).collect(Collectors.toList());
			}
		}
		return new ArrayList<>();
	}
	

	@Override
	@Transactional
	public BaseDto delete(Long id) {
		userAddressRepository.deleteById(id);
		return new BaseDto();
	}
	
	

}
