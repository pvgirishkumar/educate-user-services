package com.eductate.user.service;

import java.util.Map;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.eductate.user.dto.BaseDto;
import com.eductate.user.dto.ChangePasswordDto;
import com.eductate.user.dto.UserDto;
import com.eductate.user.utils.EduConstants;


public interface IUserService extends UserDetailsService, EduConstants {
	
	Map<String, Boolean> checkUserExist(String email, String mobileNumber);

	Object registerUser(String country, String language, UserDto userVo);
	
	UserDto getUserDetails(String userKey);
	
	UserDto updateUserProfile(UserDto userVo);
	
	UserDto forgotPassword(String country, String language, UserDto source);
	
	BaseDto resetPassword(ChangePasswordDto source);
	
	BaseDto changePassword(ChangePasswordDto source, String userKey);
	
	BaseDto updateCustomerId(String email, String customerId);
	
	UserDto findByEmail(String email);
	
	Object activateUser(String activationToken);
	
	BaseDto sendActivationEmail(String country, String language, String email, String baseUrl);
	
	UserDto checkUserExist(UserDto userVo);
	
}
