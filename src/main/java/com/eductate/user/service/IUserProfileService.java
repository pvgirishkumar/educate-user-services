package com.eductate.user.service;

import com.eductate.user.dto.UserProfileDto;

public interface IUserProfileService {
	
	UserProfileDto createUserProfile(UserProfileDto source); 
	
	UserProfileDto updateUserProfile(UserProfileDto source);
	
	UserProfileDto getUserProfile(String userKey); 

}
