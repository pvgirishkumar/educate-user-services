package com.eductate.user.service;


import java.util.List;

import com.eductate.user.dto.BaseDto;
import com.eductate.user.dto.UserAddressDto;
import com.eductate.user.utils.EduConstants;


public interface IUserAddressService extends EduConstants {
	
	UserAddressDto saveAddress(UserAddressDto source);
	
	List<UserAddressDto> getUserAddresses(String userId);
	
	BaseDto delete(Long id);

}
