package com.eductate.user.utils;

import org.springframework.util.StringUtils;

import com.eductate.user.dto.BaseDto;
import com.eductate.user.dto.UserAddressDto;
import com.eductate.user.dto.UserDto;
import com.eductate.user.dto.UserProfileDto;
import com.eductate.user.enums.UserType;
import com.eductate.user.model.BaseObject;
import com.eductate.user.model.User;
import com.eductate.user.model.UserAddress;
import com.eductate.user.model.UserProfile;


public class BeanConverter {

	private BeanConverter() {

	}
	
	private static void toBaseDto(BaseObject source, BaseDto target) {
		if(source != null && target != null) {
			target.setId(source.getId());
			target.setLastUpdatedOn(source.getLastUpdatedOn());
			target.setCreatedOn(source.getCreatedOn());
		}
	}
	
	public static void copyUserAddressDto(UserAddressDto source, UserAddress target) {
		if(target != null) {
			target.setAddressAddition(source.getAddressAddition());
			target.setCarrierBranchId(source.getCarrierBranchId());
			target.setCarrierCustomerId(source.getCarrierBranchId());
			target.setCity(source.getCity());
			target.setCountry(source.getCountry());
			target.setEmail(source.getEmail());
			target.setFirstName(source.getFirstName());
			target.setHouseNumber(source.getHouseNumber());
			target.setLastName(source.getLastName());
			target.setName(source.getName());
			target.setPhone(source.getPhone());
			target.setPostalCode(source.getPostalCode());
			target.setPostBox(source.getPostBox());
			target.setPurpose(source.getPurpose());
			target.setState(source.getState());
			target.setStreet(source.getStreet());
			target.setUserKey(source.getUserKey());
		}
	}
	
	public static UserAddressDto toUserAddressDto(UserAddress source) {
		UserAddressDto target = null;
		if(source != null) {
			target = new UserAddressDto();
			toBaseDto(source, target);
			target.setAddressAddition(source.getAddressAddition());
			target.setCarrierBranchId(source.getCarrierBranchId());
			target.setCarrierCustomerId(source.getCarrierBranchId());
			target.setCity(source.getCity());
			target.setCountry(source.getCountry());
			target.setEmail(source.getEmail());
			target.setFirstName(source.getFirstName());
			target.setHouseNumber(source.getHouseNumber());
			target.setLastName(source.getLastName());
			target.setName(source.getName());
			target.setPhone(source.getPhone());
			target.setPostalCode(source.getPostalCode());
			target.setPostBox(source.getPostBox());
			target.setPurpose(source.getPurpose());
			target.setState(source.getState());
			target.setStreet(source.getStreet());
			target.setUserKey(source.getUserKey());
		}
		return target;
	}
	
	public static UserProfileDto toUserProfileDto(UserProfile source) {

		UserProfileDto target = null;
		if (source != null) {
			target = new UserProfileDto();
			toBaseDto(source, target);
			target.setUserKey(source.getUserKey());
		}
		return target;

	}
	
	public static UserDto toUserDto(User source) {
		final UserDto target = new UserDto();
		if (source != null) {
			toBaseDto(source, target);
			target.setEmail(source.getEmail());
			target.setFirstName(source.getFirstName());
			target.setLastName(source.getLastName());
			target.setUserKey(source.getUserKey());
			target.setPhoneNumber(source.getPhoneNumber());
			target.setGender(source.getGender());
			target.setDateofBirth(source.getDateOfBirth());
			target.setPhoneNumber2(source.getPhoneNumber2());
			target.setCustomerId(source.getCustomerId());
			target.setActive(source.getActive());
			target.setUserType(StringUtils.hasText(source.getUserType().name()) ? source.getUserType() : UserType.USER);

			if (source.getRoles() != null && (!source.getRoles().isEmpty())) {
				source.getRoles().forEach(role -> target.addRole(role.getRole()));
			}

		}
		return target;
	}
	
	public static User toUser(UserDto source, User target) {
		if (source != null) {
			if(source.getUserKey() != null) {
				target.setUserKey(source.getUserKey());
			}
			target.setEmail(source.getEmail());
			target.setFirstName(source.getFirstName());
			target.setLastName(source.getLastName());
			target.setUserKey(source.getUserKey());
			target.setPhoneNumber(source.getPhoneNumber());
			target.setGender(source.getGender());
			target.setDateOfBirth(source.getDateofBirth());
			target.setPhoneNumber2(source.getPhoneNumber2());

	//		target.setUserType(StringUtils.hasText(source.getUserType().name()) ? source.getUserType() : UserType.USER);
		}
		return target;
	}
	
	public static UserDto toBaseUserDto(User source) {
		final UserDto target = new UserDto();
		if (source != null) {
			target.setFirstName(source.getFirstName());
			target.setEmail(source.getEmail());
			target.setLastName(source.getLastName());
			target.setUserKey(source.getUserKey());
			target.setPhoneNumber(source.getPhoneNumber());
			target.setPhoneNumber2(source.getPhoneNumber2());
			target.setGender(source.getGender());
			target.setDateofBirth(source.getDateOfBirth());
			target.setCustomerId(source.getCustomerId());
			target.setActive(source.getActive());
			target.setTitle(source.getTitle());

	//		target.setUserType(StringUtils.hasText(source.getUserType().name()) ? source.getUserType() : UserType.USER);
		}
		return target;
	}
}
