package com.eductate.user.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.eductate.user.enums.Role;
import com.eductate.user.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	@Query("select u from User u join u.userAuths auth where UPPER(auth.authProviderId)=UPPER(:userName)")
	Optional<User> findByUserName(@Param("userName") String userName);

	@Query("select u from User u where u.userKey=:userKey")
	Optional<User> findByUserKey(@Param("userKey") String userKey);

	@Query("select u from User u where u.email=:email")
	Optional<User> findByEmail(@Param("email") String email);

	@Modifying
	@Query("UPDATE User set userKey=:uniqueId where id=:id")
	void updateUniqueId(@Param("uniqueId") String uniqueId, @Param("id") Long id);

	@Query("select u.user from UserRole u where u.role=:role order by u.createdOn desc")
	List<User> getUsersByRole(@Param(value = "role") Role role);

	@Query("select u from User u where u.userKey in :userKeys")
	List<User> getUsersByKey(@Param("userKeys") List<String> userKeys);
	
	@Query("select u from User u where u.activationToken=:activationToken")
	Optional<User> findByActivationToken(@Param("activationToken") String activationToken);

}
