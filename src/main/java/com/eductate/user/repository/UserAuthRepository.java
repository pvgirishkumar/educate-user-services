package com.eductate.user.repository;

import java.util.Date;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.eductate.user.model.UserAuth;

@Repository
public interface UserAuthRepository extends JpaRepository<UserAuth, Long> {
	
	@Modifying
	@Query("UPDATE UserAuth SET tempPassword=:password, lastUpdatedOn=:date, resetRequested=true where user.id=:userId and active=true")
	void setTempPassword(@Param("userId") Long userId, @Param("password") String password, @Param("date") Date date);
	
	@Modifying
	@Query("UPDATE UserAuth SET active=false, lastUpdatedOn=:date where user.id=:userId and active=true")
	void deActivateAuth(@Param("userId") Long userId, @Param("date") Date date);
	
	@Query("SELECT ua from UserAuth ua where ua.user.id=:userId and ua.active=true")
	Optional<UserAuth> findActiveAuth(@Param("userId") Long userId);
	
	@Query("SELECT ua from UserAuth ua where ua.user.userKey=:userKey and ua.active=true")
	Optional<UserAuth> findActiveAuth(@Param("userKey") String userKey);
	
	Optional<UserAuth> findByTempPassword(String tempPassword);

}
