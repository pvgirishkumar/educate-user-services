package com.eductate.user.security;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;


@Configuration
@EnableWebSecurity
@Order(2)
public class CommonSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		List<RequestMatcher> requestMatchers = new ArrayList<RequestMatcher>();

		for (String path : anonymousRequestEndpoints()) {
			requestMatchers.add(new AntPathRequestMatcher(path));
		}

		http.requestMatcher(new OrRequestMatcher(requestMatchers)).csrf().disable().sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
				// handle anconversation_member authorized attempts
				.exceptionHandling()
				.authenticationEntryPoint((req, rsp, e) -> rsp.sendError(HttpServletResponse.SC_UNAUTHORIZED)).and()
				.authorizeRequests().antMatchers(anonymousRequestEndpoints()).permitAll()
				//.antMatchers("/**/admin/**").hasRole("ADMIN")
				// Any other request must be authenticated
				.anyRequest().authenticated();

	}
	

	private String[] anonymousRequestEndpoints() {
		return new String[] { 
				"/autoconfig", "/beans", "/configprops","/env" , "/mappings", "/metrics", "/shutdown", "/actuator/**", 
				"/v2/api-docs", "/swagger-resources/configuration/ui", "/swagger-resources",
				"/swagger-resources/configuration/security", "/swagger-ui.html", "/webjars/**"
		};
	}
	
}
