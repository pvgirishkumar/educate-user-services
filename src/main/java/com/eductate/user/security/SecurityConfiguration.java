package com.eductate.user.security;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.jasypt.util.password.StrongPasswordEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.filter.OAuth2ClientContextFilter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import com.eductate.user.service.IUserService;



@Configuration
@EnableOAuth2Client
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Order(1)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private IUserService userService;

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		List<RequestMatcher> requestMatchers = new ArrayList<>();
		for (String path : anonymousRequestEndpoints()) {
			requestMatchers.add(new AntPathRequestMatcher(path));
		}

		http.requestMatcher(new OrRequestMatcher(requestMatchers)).csrf().disable().exceptionHandling()
				.authenticationEntryPoint(
						(request, response, authException) -> response.sendError(HttpServletResponse.SC_UNAUTHORIZED))
				.and().authorizeRequests().antMatchers(anonymousRequestEndpoints()).permitAll().
				//antMatchers("/**/admin/**").hasRole("ADMIN").
				anyRequest()
				.authenticated().and()
                .exceptionHandling()
                    .authenticationEntryPoint(new RestAuthenticationEntryPoint());
	}

	@Autowired
	OAuth2ClientContext oauth2ClientContext;

	@Bean
	public FilterRegistrationBean<OAuth2ClientContextFilter> oauth2ClientFilterRegistration(
			OAuth2ClientContextFilter filter) {
		FilterRegistrationBean<OAuth2ClientContextFilter> registration = new FilterRegistrationBean<>();
		registration.setFilter(filter);
		registration.setOrder(-100);
		return registration;
	}

	public String[] anonymousRequestEndpoints() {
		return new String[] { "/findByUserName/*", "/user-exist", "/*/register", "/authenticate/**", "/forgot-password","/reset-password", "/internal/*",
				"/signed-cookies/**", "oauth2/**", "/login/**", "/internal/**", "/users/anonymous/notify-me", "/feedback",
				"/config", "/reset-user-token", "/v1/monitor/**",  "/analytics/add-event", "/countries", "/current-country", "/current-location", "/cities/**",
				"/activate-user", "/send-activation-email", "/signin", "/unsubscribe", "/early-bird-user", "/user/find-by-email/*", "/user/update-customer-id",
				"/user/user-exist"};
	}

	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userService).passwordEncoder(passwordEncoder());
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new PasswordEncoder() {
			final StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();

			@Override
			public boolean matches(CharSequence rawPassword, String encodedPassword) {
				return passwordEncryptor.checkPassword(String.valueOf(rawPassword), encodedPassword);
			}

			@Override
			public String encode(CharSequence rawPassword) {
				return passwordEncryptor.encryptPassword(String.valueOf(rawPassword));
			}
		};
	}

}