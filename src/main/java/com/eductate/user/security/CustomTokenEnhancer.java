package com.eductate.user.security;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.util.StringUtils;

import com.eductate.user.dto.UserDto;



public class CustomTokenEnhancer extends JwtAccessTokenConverter {
	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
		UserDto user = (UserDto) authentication.getPrincipal();
		Map<String, Object> info = new LinkedHashMap<String, Object>(accessToken.getAdditionalInformation());
		info.put("id", user.getUserKey());
		
		info.put("firstName", user.getFirstName());
		info.put("lastName", user.getLastName());
		
		info.put("userType",StringUtils.hasText(user.getUserType().name()) ? user.getUserType():"USER");
		info.put("email", user.getEmail());
		info.put("customerId", user.getCustomerId());
		info.put("active", user.getActive());

		//info.put("status", new Status(BaseDto.SUCCESS_CODE, BaseVo.SUCCESS, false));
		
		DefaultOAuth2AccessToken customAccessToken = new DefaultOAuth2AccessToken(accessToken);
		customAccessToken.setAdditionalInformation(info);
		return super.enhance(customAccessToken, authentication);
	}
	
     @Override
     public OAuth2Authentication extractAuthentication(Map<String, ?> map) {
         OAuth2Authentication auth = super.extractAuthentication(map);
         auth.setDetails(map); //this will get spring to copy JWT content into Authentication
         return auth;
     }
}
