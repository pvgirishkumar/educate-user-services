package com.eductate.user.security;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.provider.OAuth2RequestFactory;
import org.springframework.security.oauth2.provider.endpoint.TokenEndpointAuthenticationFilter;

import com.eductate.user.dto.Status;
import com.eductate.user.exception.UserNotFoundException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class CustomTokenEndpointAuthenticationFilter extends TokenEndpointAuthenticationFilter {

	private static final Logger LOGGER = LoggerFactory.getLogger(TokenEndpointAuthenticationFilter.class);
  
    @Autowired
    private final ObjectMapper mapper = new ObjectMapper();


    public CustomTokenEndpointAuthenticationFilter(AuthenticationManager authenticationManager,
            OAuth2RequestFactory oAuth2RequestFactory) {
        super(authenticationManager, oAuth2RequestFactory);
    }

    @Override
    protected void onSuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
            Authentication authResult) throws IOException {
        

    }

    @Override
    protected void onUnsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
            AuthenticationException failed) throws IOException {
    	
    	 final Map<String, Object> mapBodyException = new HashMap<>();
    	 String errorMessage = "Invalid credential";
    	 Integer errorCode = 401;
    	 if(failed.getCause() instanceof UserNotFoundException) {
    		 errorMessage = "User not found";
    		 errorCode = 1004;
    	 } else {
    		 
    	 }
    	 LOGGER.info("Request headers: {}", failed.getCause());

         
         mapBodyException.put("responseCode", errorCode);
         mapBodyException.put("message", errorMessage);
         mapBodyException.put("timestamp", (new Date()).getTime());
         mapBodyException.put("status",
                 new Status(errorCode, errorMessage, true));
         response.setContentType("application/json");
         response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
         mapper.writeValue(response.getOutputStream(), mapBodyException);
        

    }

}
