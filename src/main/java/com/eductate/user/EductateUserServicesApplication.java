package com.eductate.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EductateUserServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(EductateUserServicesApplication.class, args);
	}

}
