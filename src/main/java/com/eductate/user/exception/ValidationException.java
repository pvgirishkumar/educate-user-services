package com.eductate.user.exception;

import org.springframework.http.HttpStatus;

public class ValidationException extends CSException {
	private static final long serialVersionUID = APP_SERIAL_ID;
	
	public ValidationException() {
		super("Required values not found");
		message = "Required values not found";
	}

	public ValidationException(String message) {
		super(message);
	}
	
	public HttpStatus getHttpStatus() {
		return HttpStatus.PRECONDITION_FAILED;
	}
}
