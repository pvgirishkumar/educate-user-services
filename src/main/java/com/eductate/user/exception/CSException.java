package com.eductate.user.exception;

import org.springframework.http.HttpStatus;

import com.eductate.user.utils.EduConstants;


public class CSException extends RuntimeException implements EduConstants {
	private static final long serialVersionUID = APP_SERIAL_ID;

	protected String message;

	public CSException() {
		super("Unknown system exception");
		message = "Unknown system exception";
	}

	public HttpStatus getHttpStatus() {
		return HttpStatus.CONFLICT;
	}

	public CSException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		this.message=message;
	}

	public CSException(String message, Throwable cause) {
		super(message, cause);
		this.message=message;
	}

	public CSException(String message) {
		super(message);
		this.message=message;
	}

	public CSException(Throwable cause) {
		super(cause);
	}

	public int getErrorCode() {
		return ERROR_CODE_UNKOWN;
	}

	public String getShortMessage() {
		return message;
	}

	public String getStackTraceAsString() {
		StringBuffer buffer = new StringBuffer();
		if (this.getStackTrace() != null) {
			for (StackTraceElement element : this.getStackTrace()) {
				buffer.append(element.toString());
			}
		}
		return new String(buffer);
	}
}
